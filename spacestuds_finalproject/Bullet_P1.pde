class Bullet_P1{
  
  int x,y;
  int bulletSize = 8;
  float vel;
  int shot;
  
  Bullet_P1(int xpos, int ypos, float vel, int shot) {
    x = xpos + 32/2 - 6;
    y = ypos;
    this.vel = vel;
    this.shot = shot;
  }
  

  void draw() {
    fill(37, 150, 190);
    stroke(1);
    rect(x, y, bulletSize - 3, bulletSize + 3);
    y -= vel;
    if(y < -10){  //Remove item from list to save memory
      int idx = ammunition_P1.indexOf(this);
      ammunition_P1.remove(idx);
    }
  }
  
}
