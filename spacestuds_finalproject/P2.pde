class P2{
  int x,y;
  color nextC = color(255,255,0);
  int hp = 3;
  int delay = 0;
  boolean shooting = true;
  float dx = 5;
  int delayTime = 20;
  boolean shield = false;
  PImage ship = loadImage("spaceshipVersus.png");
  PImage shieldImg = loadImage("shield.png");
  float rot = 0;
  
  P2(){
    x = width/2 - 20;
    y = 0;
  }
  
  void draw(){
    if(hp > 0){
      drawS(x,y);
      updateObj();
    }
  }
  
  void drawS(int xpos, int ypos){
    fill(nextC);
    
    imageMode(CORNER);
    image(ship, xpos, ypos, 40,40);
    
    if(shield){
      pushMatrix();
      translate(xpos + 20, ypos + 20);
      rotate(rot);
      rot = (rot + 0.1) % (2*PI);
      imageMode(CENTER);
      image(shieldImg, 0, 0, 70, 70);
      popMatrix();
    }
  } 
  
  void updateObj() {
    if(keyPressed){
      switch(key){
      case 'a':
      if (x>0){
      x -= dx;}
      break;
      case 'd':
      if (x<width-50){
      x += dx;}
      break;
      case 's':
      if(shooting){
        ammunition_P2.add(new Bullet_P2(x, y, 6, 1));
        shootingaudio.play();
        
        shooting = false;
        delay = 0;
      }
      break;
      }
    }
    delay ++;
    if (delay >= delayTime){
      shooting = true;
    }
  }
  
  void checkHit(){
    for(int i=0; i < ammunition_P1.size(); i++){
      Bullet_P1 bullet = (Bullet_P1) ammunition_P1.get(i);
      if(bullet.x < this.x + 40 && bullet.x > this.x){
        if(bullet.y > this.y && bullet.y < y + 40){
          ammunition_P1.remove(i);
          if (bullet.shot == 1) {
           score1.score += 500;  
          }
          if(shield){
            shield = false;
          }
          else{
            this.hp -= 1;
          }
          // must check if the bullet is from the other player
        }

      }
    }
  }
}
