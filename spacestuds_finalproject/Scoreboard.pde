class Scoreboard {
  
  int xpos, ypos, score;
  
  Scoreboard(int x, int y, int score) {
    xpos = x;
    ypos = y;
    this.score = score;
    
  }
  
  void display() {
    
    textSize(32);
    text("Score " + this.score, xpos, ypos);
    
  }
  

}
