To run the animation:

1. open spacestuds_finalproject.pde file
2. Click "Run" to run animation.
3. Use up and down arrow key to scroll between ‘play game’, ‘instructions’, and ‘settings’.
4. 	If you press enter on the play game button a new game of 2 player competitive space invaders will start.
	If you press enter on the settings button it’ll allow you to toggle music and sfx on and off.
	If you press enter on the instructions button it’ll show you all the controls of the game.
5. The keyboard functionality of the game are as follows:
	Key ‘A’ - Player 2 move left
	Key ‘D’ - Player 2 move right
	Key ‘S’ - Player 2 shoot

	Key ‘Left Arrow’ - Player 2 move left
	Key ‘Right Arrow’ - Player 2 move right
	Key ‘Up Arrow - Player 2 shoot

	Key ‘M’ - Mute/ Unmute Music
	Key ‘P’ - Pause and Play Game
	Key ‘E’ - Exit to main menu from ALL screens
6. The objective of this game is to race against the other player to kill the aliens on your side of the screen to gain points, kill the other player 3 times, and trying to avoid the other player and aliens killing you. Therefore, once you start the game gain the most points to win the game. 
7. Once a player wins you can either press on the ‘replay’ button to restart the game instantly or the ‘back to main menu’ button to return to the main menu.