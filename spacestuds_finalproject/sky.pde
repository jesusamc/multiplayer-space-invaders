PShape galaxies;
float scale = 1;
float scaleDiff = 0.5;
float rotation = 0;
class sky {
  
  float x, y, r;
  color c;
  
  sky(float x, float y, float r, color c) {
    this.x = x;
    this.y = y;
    this.r = r;
    this.c = c;
  }
  
  void display() {
    
    
    galaxies = createShape(GROUP);
    fill(this.c);
    stroke(this.c);
    strokeWeight(3);
    PShape star = createShape(ELLIPSE, this.x, this.y, this.r, this.r);
    galaxies.addChild(star);
    PShape starlight = createShape(LINE, this.x, this.y, this.x, this.y - this.r);
    PShape starlight1 = createShape(LINE, this.x, this.y, this.x, this.y + this.r);
    PShape starlight2 = createShape(LINE, this.x, this.y, this.x + this.r, this.y);
    PShape starlight3 = createShape(LINE, this.x, this.y, this.x - this.r, this.y);
    PShape starlight4 = createShape(LINE, this.x, this.y, this.x + this.r, this.y + this.r);
    PShape starlight5 = createShape(LINE, this.x, this.y, this.x - this.r, this.y - this.r);
    PShape starlight6 = createShape(LINE, this.x, this.y, this.x + this.r, this.y - this.r);
    PShape starlight7 = createShape(LINE, this.x, this.y, this.x - this.r, this.y + this.r);
    galaxies.addChild(starlight); 
    galaxies.addChild(starlight1);
    galaxies.addChild(starlight2);
    galaxies.addChild(starlight3);
    galaxies.addChild(starlight4);
    galaxies.addChild(starlight5);
    galaxies.addChild(starlight6);
    galaxies.addChild(starlight7);
    

    pushMatrix();
    
    translate(this.x,this.y);
    rotate(rotation);
    rotation +=0.0001;
    scale(scaleDiff);
    scaleDiff += 0.0005;
    
    if (scaleDiff > 0.5) {
      scaleDiff *= -1 ;
    }
    
    shapeMode(CENTER);
    shape(galaxies, -x, -y);
    
    popMatrix();
    
  }
 
  
  
  
}
