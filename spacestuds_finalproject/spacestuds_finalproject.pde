P1 player1;
P2 player2;
ArrayList ammunition_P1 = new ArrayList();
ArrayList ammunition_P2 = new ArrayList();
Team_P1[] T1 = new Team_P1[30];
Team_P2[] T2 = new Team_P2[30];
Life[] hearts = new Life[3];
Life[] hearts2 = new Life[3];

int x = 0;

boolean pause = true; 
Scoreboard score1;
Scoreboard score2;

Button onbutton; //Music ON
Button offbutton;
boolean check = true;
boolean check2 = true;

Button sfxonbutton; //SFX ON
Button sfxoffbutton;

Button backbutton; //backbutton

Button replaybutton;
Button returntoMainbutton;

Button exitinstructions;



import processing.sound.*;
SoundFile shootingaudio;
SoundFile space;

int gameState = 0; //Variable used to see which screen is displayed

//Main Menu vars
int selectMainMenu = 0;
boolean changed = false;

Asteroid ast;

// input, output scores
Table table;
IntList recordedScores = new IntList(); 

// background 
sky[] gObj = new sky[50];


void setup() {
  background (0);
  size(800, 1000);

  ast = new Asteroid((width/2.0) + 100, -50, 3, 0);
  surface.setResizable(true);
  shootingaudio = new SoundFile(this, "shoot.wav");
  space = new SoundFile(this, "space.wav");
  space.loop();
  space.amp(0.1);
  
  onbutton = new Button(width/2-100, height/2-110, 100,32, "ON", 0,200,200);
  offbutton = new Button(width/2+100, height/2-110, 100,32, "OFF", 0,200,200);
  sfxonbutton = new Button(width/2-100, height/2-60, 100,32, "ON", 0,200,200);
  sfxoffbutton = new Button(width/2+100, height/2-60, 100,32, "OFF", 0,200,200);
  
  backbutton = new Button(width/2, height/2+200, 100,32, "START GAME", 200,0,200);

  
  
  replaybutton = new Button(width/2 - 200, height - 180, 400, 50, "REPLAY", 0, 200, 200);
  returntoMainbutton = new Button(width/2 - 200, height - 120, 400, 50, "MAIN MENU", 0, 200, 200);
  
  exitinstructions = new Button(width/2-100, height/2+370, 200,50, "START GAME", 0, 200, 200);
  
  table = loadTable("highscores.csv", "header");
    for (TableRow row : table.rows()) {
      int score = row.getInt("highscore");
      recordedScores.append(score);
    }  
    
   for (int i = 0; i < 50; i++) {
    sky lg;
    lg = new sky(random(800), random(1000), random(10,15), color(255));
    gObj[i] = lg;
     
  }
 
}

void draw() {
  switch (gameState) {
  case 0:  //Main screen
    mainScreen();
    break;
  case 1: //Settings
    SettingsScreen();
    break;
  case 2: // Instructions
    InstructionsScreen();
    break;
  case 3: //Game screen
    PlayGame();
    break;
  case 4: //End screen
    winScreen();
    break;
  case 5: //replay + reset
    newGame();
  }
  
  

  //Muting Music
  if (keyPressed) 
  {
    if (key == 'm') 
    {
      if (space.isPlaying()) 
      {
        space.pause();
      } 
      else 
        {
          space.play();
        }
     }
  }
}

void mainScreen() {
  
  //Main title
  background(0);
  
    for (int i = 0; i < gObj.length; i++) {    
    gObj[i].display(); 
  }
  
  textAlign(CENTER);
  textSize(100);
  fill(255);
  text("SPACE", width/2, height/2 - 100);
  text("STUDS", width/2, height/2);

  //Options
  textSize(40);
  text("Play", width/2, height/2 + 100);
  text("Settings", width/2, height/2 + 150);
  text("Instructions", width/2, height/2 + 200);




  //Prevent the select circle from scrolling
  if (changed) {
    if (!keyPressed) {
      changed = !changed;
    }
  }

  //Main menu controls
  if (keyPressed && !changed) {
    if (keyCode == DOWN) {
      selectMainMenu = (selectMainMenu + 1)%3;
      changed = true;
    } else if (keyCode == UP) {
      selectMainMenu -= 1;
      if (selectMainMenu < 0) {
        selectMainMenu = 2;
      }
      changed = true;
    }
  }

  //Selections
  switch(selectMainMenu) {
  case 0:  //Play
    ellipse(width/2 - 150, height/2 + 85, 20, 20);
    if (keyCode == ENTER) {
      //Setting enemies
      for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 3; j++) {
          int idx = i + 10*j;
          T1[idx] = new Team_P1(130 + 60*i, 200 + 60*j);
          T2[idx] = new Team_P2(130 + 60*i, height/2 + 200 + 60*j);
        }
      }
      //Reseting players
      player1 = new P1();
      player2 = new P2();
      player2.shield = false;
      
      // reset the game
      score2 = new Scoreboard(130, 35, 0);
      score1 = new Scoreboard(130, height - 35, 0);
      
      int xLife = 0; int yLife = 0;
      int yLife2 = height/2 + 375;
      for (int j = 0; j < 3; j++) {
        hearts[j] = new Life(xLife, yLife);
        yLife += 33;
        hearts2[j] = new Life(xLife, yLife2);
        yLife2 += 33;
  
      }
      
      gameState = 3;
    }
    break;
  case 1:  //Settings
    ellipse(width/2 - 150, height/2 + 135, 20, 20);
    if (keyCode == ENTER) {
      gameState = 1;
    }
    break;
  case 2:  //Instructions
    ellipse(width/2 - 150, height/2 + 185, 20, 20);
    if (keyCode == ENTER) {
      //Reseting players
      player1 = new P1();
      player2 = new P2();
      gameState = 2;
    }
    break;
  }
  
}

void SettingsScreen() {
  background(0);
  
    for (int i = 0; i < gObj.length; i++) {    
    gObj[i].display(); 
  }
  
  fill(255);
  textSize(64);
  text("SETTINGS", width/2, height/2-200); 

  textSize(16);

  onbutton.update();
  onbutton.render();
  onbutton.hover();
  
  offbutton.update();
  offbutton.render();
  offbutton.hover();
  
  sfxonbutton.update();
  sfxonbutton.render();
  sfxonbutton.hover();
  
  sfxoffbutton.update();
  sfxoffbutton.render();
  sfxoffbutton.hover();
  
  backbutton.update();
  backbutton.render();
  backbutton.hover();
  
  
  //Music Buttons
  if (onbutton.isClicked())
  {
    space.amp(0.1);
    offbutton.Clicked = false;
    check = true;
    
    
  }
  
  if (offbutton.isClicked())
  {
    space.amp(0); 
    onbutton.Clicked = false;
    check = false;
    
  }
  
  //SFX Buttons
  if (sfxonbutton.isClicked())
  {    
    shootingaudio.amp(1);
    sfxoffbutton.Clicked = false;
    check2 = true;
    
    
  }
  
  if (sfxoffbutton.isClicked())
  {
    
    shootingaudio.amp(0);
    sfxonbutton.Clicked = false;
    check2 = false;
    
  }
  
  
  if (backbutton.isClicked()) //return to main screen -- dot (selectMainMenu) remains on setting in main menu, 
                              // must figure out how to reset dot back to initialized point
  { 
    gameState = 0;
    mainScreen();
    selectMainMenu = 0;
    println(selectMainMenu);
    mainScreen();

  }
  
  textAlign(CENTER,CENTER);
  
  //Music ind
  if (check==true)
  {
  fill(0,255,0);
  text("ON", width/2-160, height/2-104);
  }
  
  else{
  fill(255,0,0);
  text("OFF", width/2-160, height/2-104);
  }
  
  
  //SFX ind
  if (check2==true)
  {
  fill(0,255,0);
  text("ON", width/2-160, height/2-44);
  }
  
  else{
  fill(255,0,0);
  text("OFF", width/2-160, height/2-44);
  }
  

  
  
  
  textAlign(CENTER,CENTER);
  fill(255);
  text("MUSIC", width/2-200, height/2-104);
  text("SFX", width/2-200, height/2-44);
  
  
}



void InstructionsScreen() {
  background(0);
  
    for (int i = 0; i < gObj.length; i++) {    
    gObj[i].display(); 
  }

  rectMode(CENTER);
  fill(255, 255, 102);
  rect(width/2, height/2, width, 20);

  //Show controls
  textAlign(CENTER);
  textSize(32);
  fill(35, 148, 235);
  //P2
  text("A: Move left", width/2, height/2 - 200);
  text("D: Move right", width/2, height/2 - 150);
  text("S: Shoot", width/2, height/2 - 100);
  //P1
  text("Left arrow: Move left", width/2, height/2 + 100);
  text("Right arrow: Move right", width/2, height/2 + 150);
  text("Up arrow: Shoot", width/2, height/2 + 200);

  text("M key: Mute music", width/2, height/2 + 250);
  text("P key: Pause/Play Game", width/2, height/2 + 300);
  text("X key: Exit Game", width/2, height/2 + 350);
  
  
  if (exitinstructions.isClicked()) 
                             
  { 
    gameState = 0;
    mainScreen();
    selectMainMenu = 0;
    println(selectMainMenu);
    mainScreen();

  }

  //Show players
  player1.draw();
  player2.draw();

  //Show bullets
  for (int i = 0; i < ammunition_P1.size(); i++) {
    Bullet_P1 bullet = (Bullet_P1) ammunition_P1.get(i);
    bullet.draw();
  }
  for (int i = 0; i < ammunition_P2.size(); i++) {
    Bullet_P2 bullet = (Bullet_P2) ammunition_P2.get(i);
    bullet.draw();
  }
  
  rectMode(CORNER);
  exitinstructions.render();
  exitinstructions.update();
  exitinstructions.hover();
  
  
}

void PlayGame() {
  background(0);
  
    for (int i = 0; i < gObj.length; i++) {    
    gObj[i].display(); 
  }
  
  strokeWeight(1);
  stroke(1);
  rectMode(CENTER);
  fill(255, 255, 102);
  rect(width/2, height/2, width, 20);
  
  score1.display();
  score2.display();
  
  
  for (int i = 0; i < player1.hp; i++) {
    hearts2[i].display();
  }
  
  for (int i = 0; i < player2.hp; i++) {
    hearts[i].display();
  }

  //Show players
  player1.draw();
  player1.checkHit();

  player2.draw();
  player2.checkHit();

  ast.show();
  ast.checkHit();
  ast.move();
  if (ast.hit == 2 && floor(random(1, 1000)) == 1) {
    if (floor(random(1, 3)) == 1) { //Appear on left
      ast.x = -50;
      ast.y = random(height/2 - 100, height/2 + 100);
      ast.dx = cos(random(-PI/6, PI/6));
      ast.dy = -sin(random(-PI/6, PI/6));
      ast.hit = 0;
      ast.type = floor(random(1, 4));
      
    } else {  //Appear on right
      ast.x = width + 50;
      ast.y = random(height/2 - 100, height/2 + 100);
      ast.dx = cos(random(5*PI/6, 7*PI/6));
      ast.dy = -sin(random(5*PI/6, 7*PI/6));
      ast.hit = 0;
      ast.type = floor(random(1, 4));
    }
  }

  //Show aliens
  for (int i = 0; i < T1.length; i++) {
    T1[i].show();
    T1[i].move();
    T1[i].shoot();
    T1[i].checkHit();
    T2[i].show();
    T2[i].move();
    T2[i].shoot();
    T2[i].checkHit();
  }

  //Show bullets
  for (int i = 0; i < ammunition_P1.size(); i++) {
    Bullet_P1 bullet = (Bullet_P1) ammunition_P1.get(i);
    bullet.draw();
  }
  for (int i = 0; i < ammunition_P2.size(); i++) {
    Bullet_P2 bullet = (Bullet_P2) ammunition_P2.get(i);
    bullet.draw();
  }

  if (player1.hp == 0 || player2.hp == 0) {
    ammunition_P1.clear();
    ammunition_P2.clear();
    gameState = 4;
  }
}

void winScreen() {
  background(0);
  
    for (int i = 0; i < gObj.length; i++) {    
    gObj[i].display(); 
  }
  
  fill(255);
  //show winning score
  textAlign(CENTER);
  textSize(64);
  text("GAME OVER", width/2, height/3);
  textSize(44);
  if (score1.score > score2.score) {
    text("WINNER: PLAYER1", width/2, height/2 - 50);
    text("SCORE: " + score1.score, width/2, height/2);    
    
    for (int i = 0; i < recordedScores.size(); i++) {
      if (!recordedScores.hasValue(score1.score)){
        recordedScores.append(score1.score);
        TableRow newRow = table.addRow();
        newRow.setInt(i, score1.score);
        saveTable(table, "highscores.csv");
      }
      
    }


  }
  
  if (score2.score > score1.score) {
    text("WINNER: PLAYER2", width/2, height/2 - 50);
    text("SCORE: " + score2.score, width/2, height/2);
    
   for (int i = 0; i < recordedScores.size(); i++) {
      if (!recordedScores.hasValue(score2.score)){
        recordedScores.append(score2.score);
        TableRow newRow = table.addRow();
        newRow.setInt(i, score2.score);
        saveTable(table, "highscores.csv");
      }    
    }
  }


  
  
  
  //list + record high scores
      text("All Time Highscores", width/2, height/2 + 60);
      recordedScores.sortReverse();

      int y = 100;
      for (int i = 0 ; i < 5; i++) {
        int scores = recordedScores.get(i);
        fill(255);
        textSize(30);
        textAlign(CENTER, CENTER);
        text(scores, width/2, height/2 + y);
        y += 35;
      }

  
  //fill(142);
  stroke(255);
  fill(255);
  textSize(30);
  textAlign(LEFT);
  
  rectMode(CORNER);
  replaybutton.update();
  replaybutton.render();
  replaybutton.hover();

  returntoMainbutton.update();
  returntoMainbutton.render();
  returntoMainbutton.hover();
  
  if (returntoMainbutton.isClicked()) {
    gameState = 0;
    mainScreen();

  }
   
  if (replaybutton.isClicked()) {
    //reset the game
    gameState = 5;
    newGame();
  }
   
}

void newGame() {
  //implement case 0 again
  for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 3; j++) {
          int idx = i + 10*j;
          T1[idx] = new Team_P1(130 + 60*i, 200 + 60*j);
          T2[idx] = new Team_P2(130 + 60*i, height/2 + 200 + 60*j);
        }
      }
      //Reseting players
      player1 = new P1();
      player2 = new P2();
      
      // reset the game
      score2 = new Scoreboard(130, 35, 0);
      score1 = new Scoreboard(130, height - 35, 0);
      
      int xLife = 0; int yLife = 0;
      int yLife2 = height/2 + 375;
      for (int j = 0; j < 3; j++) {
        hearts[j] = new Life(xLife, yLife);
        yLife += 33;
        hearts2[j] = new Life(xLife, yLife2);
        yLife2 += 33;
  
      }
      gameState = 3;
      PlayGame();
      
}

void keyPressed() {
 
  if (keyPressed && (key == 'p')) {
    pause = !pause;
    
    if (pause) {
      noLoop();
      fill(255);
      textSize(60);
      textAlign(CENTER);
      text("PAUSED", width/2, height/2);
      textSize(16);
      rectMode(CORNER);
      

    }
    else {
      loop();
    }
    
  }
  
  if (keyPressed && (key == 'x')) {
    
    gameState = 0;
    mainScreen();
    selectMainMenu = 0;
    println(selectMainMenu);
    mainScreen();
  }
  
  
}
