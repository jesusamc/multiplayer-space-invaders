class Life {
  
  int x;
  PImage lives;
  int y;
    
  Life(int x, int y) {
    this.x = x;
    this.y = y;
    lives = loadImage("heart.png");
  }
  
  void display() {
        pushMatrix();
    translate(10, 10);
    imageMode(CORNER);
    image(lives, x, y, 30, 30);
    popMatrix();
    
  }
  
}
