class P1{
  int x,y;
  color nextC = color(255,255,0);
  int hp = 3;
  int delay = 0;
  boolean shooting = true;
  float dx = 5;
  int delayTime = 20;
  boolean shield = false;
  PImage ship = loadImage("spaceship.png");
  PImage shieldImg = loadImage("shield.png");
  float rot = 0;
  
  P1(){
    x = width/2 - 20;
    y = height - 40;
  }
  
  void draw(){
    if(hp > 0){
      drawS(x,y);
      updateObj();
    }
  }
  
  void drawS(int xpos, int ypos){
    fill(nextC);
    
    imageMode(CORNER);
    image(ship, xpos, ypos, 40,40);
    
    if(shield){
      pushMatrix();
      translate(xpos + 20, ypos + 20);
      rotate(rot);
      rot = (rot + 0.1) % (2*PI);
      imageMode(CENTER);
      image(shieldImg, 0, 0, 70, 70);
      popMatrix();
    }
  } 
  
  void updateObj() {
    if(keyPressed){
      switch(keyCode){
        case LEFT:
          if (x>0){
            x -= dx;}
          break;
        case RIGHT:
          if (x<width-50){
          x += dx;}
          break;
        case UP:
          if(shooting){
            ammunition_P1.add(new Bullet_P1(x, y, 6, 1));
            shootingaudio.play();
            
            shooting = false;
            delay = 0;
        
      }
      break;
      }
    }
    delay ++;
    if (delay >= delayTime){
      shooting = true;
    }
  }
  
  void checkHit(){
    for(int i=0; i < ammunition_P2.size(); i++){
      Bullet_P2 bullet = (Bullet_P2) ammunition_P2.get(i);
      if(bullet.x < this.x + 40 && bullet.x > this.x){
        if(bullet.y > this.y && bullet.y < y + 40){
          ammunition_P2.remove(i);
          if (bullet.shot == 1) {
           score2.score += 500;  
           }
          if(shield){
            shield = false;
          }
          else{
            this.hp -= 1;
          } 
        }
      }
    }
  }
}
