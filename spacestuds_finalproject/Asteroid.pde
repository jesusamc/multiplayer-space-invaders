class Asteroid{
  PImage asteroid = loadImage("asteroid.png");
  PImage powerup = loadImage("powerup.png");
  
  float x, y;  //Coordinates
  int type;  //Type of power up
  float angle; //Angle that asteroid will take
  float rotationAng = 0;
  float dx, dy; 
  float scale = 5;
  
  int hit = 0;
  
  int time = millis();
  int powerUp = millis();
  int poweredPlayer;
  boolean powered = false;
  
  Asteroid(float x, float y, int type, float angle){
    this.x = x;
    this.y = y;
    this.type = type;
    this.dx = cos(angle);
    this.dy = -sin(angle);  //Correction for Y axis pointing down
  }
  
  void show(){
    
    if(hit == 0){
      pushMatrix();
      translate(x, y);
      rotate(rotationAng);
      rotationAng += 0.1;
      rotationAng = rotationAng % TWO_PI;
      imageMode(CORNER);
      image(asteroid, -20, -20, 40, 40);
      popMatrix();
      
    }
    else if (hit == 1){
      pushMatrix();
      translate(x - 20, y - 20);
      imageMode(CORNER);
      image(powerup, 0, 0, 40, 40);
      popMatrix();
    }
    else if(powered){
      if(millis() > powerUp + 10000){
        if(poweredPlayer == 1){
          if(player1.dx != 5){
            player1.dx = 5;
          }
          else if(player1.delayTime != 20){
            player1.delayTime = 20;
          }
          else{
            player1.shield = false;
          }
        }
        else{
          if(player2.dx != 5){
            player2.dx = 5;
          }
          else if(player2.delayTime != 20){
            player2.delayTime = 20;
          }
          else{
            player2.shield = false;
          }
        }
      }
    }
  }
  
  void move(){
    if(hit == 0){
      if(millis() > time + 30){
        x += scale*dx;
        if(x < -100 || x > width + 100){
          hit = 2;
        }
        y += scale*dy;
        time = millis();
      }
    }
    else if(hit == 1){
      if(y < height/2){
        y -= 4;
        if(y < -50){
          hit = 2;
        }
      }
      else{
        y += 4;
        if(y > height + 50){
          hit = 2;
        }
      }
    }
  }
  
  void checkHit(){
    if(hit == 0){  //Collide with bullets
      for(int i = 0; i < ammunition_P1.size(); i++){
        Bullet_P1 bullet = (Bullet_P1) ammunition_P1.get(i);
        if(bullet.x < x + 20 && bullet.x > x - 20){
          if(bullet.y < y + 20 && bullet.y > y - 20){
            ammunition_P1.remove(i);
            score1.score += 10;
            hit = 1;
          }
        }
      }
      for(int i = 0; i < ammunition_P2.size(); i++){
        Bullet_P2 bullet = (Bullet_P2) ammunition_P2.get(i);
        if(bullet.x < x + 20 && bullet.x > x - 20){
          if(bullet.y < y + 20 && bullet.y > y - 20){
            ammunition_P2.remove(i);
            score2.score += 10;
            hit = 1;
          }
        }
      }
    }
    else if (hit == 1){  //Collide with player
      if(y > player1.y && y < player1.y + 40){
        if(x > player1.x && x < player1.x + 40){
          powerUp = millis();
          powered = true;
          poweredPlayer = 1;
          hit = 2;
          if(type == 1){  //Speed
            player1.dx = 10; 
            score1.score += 25;
          }
          else if(type == 2){  //Rapid fire
            player1.delayTime = 11;
            powerUp += 5000;
            score1.score += 50;
          }
          else if(type == 3){  //Shield
            player1.shield = true;
            score1.score += 75;
          }
        }
      }
      else if(y > player2.y && y < player2.y + 40){
        if(x > player2.x && x < player2.x + 40){
            powerUp = millis();
            powered = true;
            poweredPlayer = 2;
            hit = 2;
            if(type == 1){  //Speed
              player2.dx = 10;    
              score2.score += 25;
            }
            else if(type == 2){  //Rapid fire
              player2.delayTime = 7;
              powerUp += 5000;
              score2.score += 50;
            }
            else if(type == 3){  //Shield
              player2.shield = true;
              score2.score += 75;
            }
        }
      }
    } 
  }
  
}
