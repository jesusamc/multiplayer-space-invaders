class Bullet_P2{
  
  int x,y;
  int bulletSize = 8;
  float vel;
  int shot;
  
  Bullet_P2(int xpos, int ypos, float vel, int shot) {
    x = xpos + 32/2 - 6;
    y = ypos;
    this.vel = vel;
    this.shot = shot;

  }
  

  void draw() {
    fill(255, 0, 0);
    stroke(1);
    rect(x, y, bulletSize - 3, bulletSize + 3);
    y += vel;
    if(y > height + 10){  //Remove item from list to save memory
      int idx = ammunition_P2.indexOf(this);
      ammunition_P2.remove(idx);
    }
  }
  
}
