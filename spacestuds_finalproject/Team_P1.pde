class Team_P1{
  float x, y;
  float originX, originY;
  boolean isHit = false;
  boolean state;
  
  //Horizontal movement
  PImage invader = loadImage("invaderP1.png");
  float t = 0;
  float dt = 0.03;
  int horizontalTimer = 0, horizontalFreq = 10;
  
  //Vertical movement
  int verticalTimer = 0, verticalFreq = 5000; 
  boolean movingDown = false;
  int down = 1;
  float dy = 0.1;
  
  Team_P1(float x, float y){
    this.x = x;
    this.y = y;
    this.originX = x;
    this.state = true;
  }
  
  void show(){
    if(state){
      pushMatrix();
      translate(-20, -20);
      imageMode(CORNER);
      image(invader, x, y, 40, 40);
      popMatrix();
    }
  }
  
  void move(){
    //Move horizontally in oscillation
    x = originX + lerp(-40, 40, t);
    if (millis() - horizontalTimer >= horizontalFreq){
      t += dt;
      if (t > 1 || t < 0){
        dt *= -1;
      }
      horizontalTimer = millis();
    }
    
    //Move vertically every 30 seconds
    /*
    if(movingDown){
      if(originY + 40 * down < y){
        down += 1;
        movingDown = false;
        verticalTimer = millis();
      }
      else{
        y = y + dy;
      }
    }
    else{
      if(millis() - verticalTimer >= verticalFreq){
      movingDown = true;
      verticalTimer = millis();
      }
    }
    */
  }
  
  void shoot(){
    int num = int(random(1, 2000));
    if(num < 2){
      ammunition_P1.add( new Bullet_P1 (int(x), int(y), 6, 0)); //Change the type of bullet depending on type of enemy
    }
  }
  
  void checkHit(){
    for(int i=0; i < ammunition_P2.size(); i++){
      Bullet_P2 bullet = (Bullet_P2) ammunition_P2.get(i);
      if(bullet.x < this.x + 20 && bullet.x > this.x - 20){
        if(bullet.y > this.y - 20 && bullet.y < y + 20){
          if(state){
            ammunition_P2.remove(i);
            this.state = false;
            score2.score = score2.score + 100;
          }
        }
      }
    }
  }
}
